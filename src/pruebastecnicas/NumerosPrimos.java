package pruebastecnicas;

public class NumerosPrimos {
	public static void main(String[] args) {
String primo= "";
		for(int num=2;num<=100;num ++) {
			if(esPrimo(num)) {
				primo+=num+", ";
			}
		}
		System.out.println("N�meros primos en rango del 1 al 100--> " + primo);
		
	}

	private static Boolean esPrimo(int num) {
		int ini=2;
		Boolean valida=true;
		while((valida)&&(ini!=num)) {
			if (num % ini == 0)
			      valida = false;
			    ini++;
		}
		return valida;
		
	}
}
